//
//  DroppablePin.swift
//  Pixel City
//
//  Created by Olimjon Turgunov on 19.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import Foundation
import MapKit

class DroppablePin: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var identifier: String
    
    init(coordinate: CLLocationCoordinate2D, identifier: String) {
        self.coordinate = coordinate
        self.identifier = identifier
        super.init()
    }
    
}
