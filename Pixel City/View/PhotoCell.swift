//
//  PhotoCell.swift
//  Pixel City
//
//  Created by Olimjon Turgunov on 21.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Init coder has not been implemented")
    }
}
