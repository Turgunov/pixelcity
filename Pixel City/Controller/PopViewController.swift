//
//  PopViewController.swift
//  Pixel City
//
//  Created by Olimjon Turgunov on 22.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class PopViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var popImageView: UIImageView!
    @IBOutlet weak var doubleTapNotificationView: UIView!
    
    @IBOutlet weak var imageTitleLabel: UILabel!
    @IBOutlet weak var imageDescriptionLabel: UILabel!
    @IBOutlet weak var postedLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    var passedImage: (image: UIImage, id: String)!
    
    var pixelImage = PixelImage()
    
    func initData(forImage image: (image: UIImage, id: String)) {
        passedImage = image
    }

    // MARK: - Application Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animatedFadeOut(view: doubleTapNotificationView)
    }
    
    func addDoubleTap() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(screenWasDoubleTapped))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        view.addGestureRecognizer(doubleTap)
    }
    
    @objc func screenWasDoubleTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    func animatedFadeOut(view: UIView) {
        view.alpha = 1
        UIView.animate(withDuration: 2) {
            view.alpha = 0
        }
    }
    
    func setUpView() {
        popImageView.image = passedImage.image
        imageTitleLabel.text = pixelImage.imageTitle ?? ""
        imageDescriptionLabel.text = pixelImage.imageDescription ?? ""
        postedLabel.text = "\(pixelImage.postedDate ?? "")"
        
        retrievePhotoMetadata(forPhotoId: passedImage.id) { [weak self] success in
            if success {
                self?.updateView()
                self?.centerMapOnUserLocation()
            }
        }
        
        addDoubleTap()
    }
    
    func updateView() {
        imageTitleLabel.text = pixelImage.imageTitle ?? ""
        imageDescriptionLabel.text = pixelImage.imageDescription ?? ""
        postedLabel.text = "Posted: \(pixelImage.postedDate ?? "")"
    }
    
    func retrievePhotoMetadata(forPhotoId id: String, completion: @escaping CompletionHandler) {
        Alamofire.request(getPhotoMetaUrl(forApiKey: apiKey, photoId: id)).responseJSON { [weak self] response in
            if response.result.error == nil {
                print(getPhotoMetaUrl(forApiKey: apiKey, photoId: id))
                guard let json = response.result.value as? [String : AnyObject] else { return }
                if let photoInfoDictionary = json["photo"] as? [String : AnyObject] {
                    var photoTitle = ""
                    var photoDescription = ""
                    var postedDate = ""
                    var latitude = ""
                    var longitude = ""

                    if let titleDictionary = photoInfoDictionary["title"] as? [String : AnyObject] {
                        if let title = titleDictionary["_content"] as? String {
                            photoTitle = title
                        }
                    }
                    
                    if let descriptionDictionary = photoInfoDictionary["description"] as? [String : AnyObject] {
                        if let description = descriptionDictionary["_content"] as? String {
                            photoDescription = description
                        }
                    }
                    
                    if let datesDictionary = photoInfoDictionary["dates"] as? [String : AnyObject] {
                        if let posted = datesDictionary["taken"] as? String {
                            postedDate = posted
                        }
                    }
                    
                    if let locationDictionary = photoInfoDictionary["location"] as? [String : AnyObject] {
                        if let latitudeData = locationDictionary["latitude"] as? String {
                            latitude = latitudeData
                        }
                        if let longitudeData = locationDictionary["longitude"] as? String {
                            longitude = longitudeData
                        }
                    }
                    
                    let image = PixelImage(imageTitle: photoTitle, imageDescription: photoDescription, postedDate: postedDate, latitude: latitude, longitude: longitude)
                    print(image)
                    self?.pixelImage = image
                }
                completion(true)
            }else {
                print(response.result.error as Any)
            }
        }
    }
    
    func cancelAllSessions() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
    
    func centerMapOnUserLocation() {
        guard let latitude = Double(pixelImage.latitude!) else { return }
        guard let longitude = Double(pixelImage.longitude!) else { return }

        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let coordinateRegion = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        
        let annotation = DroppablePin(coordinate: coordinate, identifier: "PhotoLocation")
        
        mapView.setRegion(coordinateRegion, animated: true)
        mapView.addAnnotation(annotation)

    }


}

