//
//  ViewController.swift
//  Pixel City
//
//  Created by Olimjon Turgunov on 19.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import AlamofireImage

class MapViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
            if (mapView.subviews[0].gestureRecognizers != nil){
                for gesture in mapView.subviews[0].gestureRecognizers!{
                    if (gesture.isKind(of: UITapGestureRecognizer.self)){
                        mapView.subviews[0].removeGestureRecognizer(gesture)
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var pullUpView: UIView!
    @IBOutlet weak var pullUpViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - UICollectionView
    var collectionView: UICollectionView? {
        didSet {
            collectionView?.delegate = self
            collectionView?.dataSource = self
        }
    }
    
    // MARK: - Location
    var locationManager = CLLocationManager()
    let authorisationStatus = CLLocationManager.authorizationStatus()
    
    // MARK: - UI Elements
    var spinner: UIActivityIndicatorView?
    var progressLabel: UILabel?
    
    // MARK: - Constants
    let regionRadius: Double = 100
    var screenSize = UIScreen.main.bounds
    
    // MARK: - Model
    var imageUrlWithId = [(url: String, id: String)]()
    var imageArray = [(image: UIImage, id: String)]()
    
    
    var pixelImage = PixelImage()
    
    // MARK: - Application Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        configureLocationServices()
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView?.register(PhotoCell.self, forCellWithReuseIdentifier: "photoCell")
        collectionView?.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.6509803922, blue: 0.137254902, alpha: 1)
        
        pullUpView.addSubview(collectionView!)
        registerForPreviewing(with: self, sourceView: collectionView!)
        addDoubleTap()
        
        centerMapOnUserLocation()
    }
    
    // MARK: - Actions
    @IBAction func centerMapButtonPressed(_ sender: UIButton) {
        if authorisationStatus == .authorizedAlways || authorisationStatus == .authorizedWhenInUse {
            centerMapOnUserLocation()
        }
    }
    
    // MARK: - Gesture Handling Function
    func addDoubleTap() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(dropPin(sender:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        mapView.addGestureRecognizer(doubleTap)
    }
    
    func addSwipe() {
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(animateViewDown))
        swipeGesture.direction = .down
        pullUpView.addGestureRecognizer(swipeGesture)
    }
    
    @objc func animateViewDown() {
        cancelAllSessions()
        pullUpViewHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func animateViewUp() {
        pullUpViewHeightConstraint.constant = 300
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - UI Elements
    func addSpinner() {
        spinner = UIActivityIndicatorView()
        spinner?.center = CGPoint(x: (screenSize.width / 2) - ((spinner?.frame.width)! / 2), y: 150)
        spinner?.style = .whiteLarge
        spinner?.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        spinner?.startAnimating()
        collectionView?.addSubview(spinner!)
    }
    
    func removeSpinner() {
        if spinner != nil {
            spinner?.removeFromSuperview()
        }
    }
    
    func addProgressLabel() {
        progressLabel = UILabel()
        progressLabel?.frame = CGRect(x: (screenSize.width / 2) - 120, y: 175, width: 240, height: 40)
        progressLabel?.font = UIFont(name: "Avenir Next", size: 14)
        progressLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        progressLabel?.textAlignment = .center
        collectionView?.addSubview(progressLabel!)
    }
    
    func removeProgressLabel() {
        if progressLabel != nil {
            progressLabel?.removeFromSuperview()
        }
    }
    
    
    // MARK: - Web requests
    func retrieveUrls(forAnnotation annotation: DroppablePin, completion: @escaping CompletionHandler) {
        Alamofire.request(getPhotosUrl(forApiKey: apiKey, withAnnotation: annotation, andNumberOfPhotos: 20)).responseJSON { [weak self] response in
            if response.result.error == nil {
                guard let json = response.result.value as? [String : AnyObject] else { return }
                if let photosDictionary = json["photos"] as? [String : AnyObject] {
                    if let photosDictionaryArray = photosDictionary["photo"] as? [[String : AnyObject]] {
                        for photo in photosDictionaryArray {
                            let id = "\(photo["id"]!)"
                            let postUrl = "https://farm\(photo["farm"]!).staticflickr.com/\(photo["server"]!)/\(photo["id"]!)_\(photo["secret"]!)_h_d.jpg"
                            
                            self?.imageUrlWithId.append((postUrl, id))
                        }
                        completion(true)
                    }
                }
            }else {
                print(response.result.error as Any)
            }
        }
    }
    
    func retrieveImages(completion: @escaping CompletionHandler) {
        for urlAndId in imageUrlWithId {
            Alamofire.request(urlAndId.url).responseImage { [weak self] response in
                if response.result.error == nil {
                    guard let image = response.result.value else { return }
                    
                    self?.imageArray.append((image, urlAndId.id))
                    self?.progressLabel?.text = "\(self?.imageArray.count ?? 0)/20 IMAGES DOWNLOADED"
                    
                    if self?.imageArray.count == self?.imageUrlWithId.count  {
                        
                        completion(true)
                    }
                }else {
                    print(response.result.error as Any)
                }
            }
        }
    }
    
    func cancelAllSessions() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }

}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        let pinAnnotation = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "droppablePin")
        pinAnnotation.pinTintColor = #colorLiteral(red: 0.9725490196, green: 0.6509803922, blue: 0.137254902, alpha: 1)
        pinAnnotation.animatesDrop = true
        
        return pinAnnotation
    }
    
    func centerMapOnUserLocation() {
        guard let coordinate = locationManager.location?.coordinate else { return }
        let coordinateRegion = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: regionRadius * 2, longitudinalMeters: regionRadius * 2)
        
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    @objc func dropPin(sender: UITapGestureRecognizer) {
        cancelAllSessions()
        
        removeProgressLabel()
        removePin()
        removeSpinner()
        
        imageArray = []
        imageUrlWithId = []
        
        collectionView?.reloadData()
        
        addProgressLabel()
        animateViewUp()
        addSwipe()
        addSpinner()
        
        let touchPoint = sender.location(in: mapView)
        let touchCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        
        let annotation = DroppablePin(coordinate: touchCoordinate, identifier: "droppablePin")
        mapView.addAnnotation(annotation)
        
        let coordianteRegion = MKCoordinateRegion.init(center: touchCoordinate, latitudinalMeters: regionRadius * 2, longitudinalMeters: regionRadius * 2)
        mapView.setRegion(coordianteRegion, animated: true)
        
        retrieveUrls(forAnnotation: annotation) { [weak self] success in
            if success {
                self?.retrieveImages { [weak self] success in
                    if success {
                        self?.removeSpinner()
                        self?.removeProgressLabel()
                        self?.collectionView?.reloadData()
                    }
                }
            }
        }
    }
    
    func removePin() {
        for annotation in mapView.annotations {
            mapView.removeAnnotation(annotation)
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func configureLocationServices() {
        if authorisationStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }else {
            centerMapOnUserLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        centerMapOnUserLocation()
    }
}

extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as? PhotoCell else { return UICollectionViewCell() }
        let imageView = UIImageView(image: imageArray[indexPath.row].image)
        imageView.contentMode = .scaleAspectFit
        cell.addSubview(imageView)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "PopViewController") as? PopViewController else { return }
        popVC.initData(forImage: (self.imageArray[indexPath.row]))
        present(popVC, animated: true, completion: nil)
    }
    
}

extension MapViewController: UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = collectionView?.indexPathForItem(at: location), let cell = collectionView?.cellForItem(at: indexPath) else { return nil }
        
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "PopViewController") as? PopViewController else { return nil }
        // PopViewController must retrieve all the items in his self Controller
        
        popVC.initData(forImage: (self.imageArray[indexPath.row]))

        previewingContext.sourceRect = cell.contentView.frame
        
        return popVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }
}

