//
//  Constants.swift
//  Pixel City
//
//  Created by Olimjon Turgunov on 22.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

typealias CompletionHandler = (_ status: Bool) -> ()

import Foundation

let apiKey = "903fb47b981b71bcaa600362da5fe8a2"

func getPhotosUrl(forApiKey key: String, withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int) -> String {
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=km&per_page=\(number)&format=json&nojsoncallback=1"
}

func getPhotoMetaUrl(forApiKey key: String, photoId id: String) -> String {
    return "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=\(apiKey)&photo_id=\(id)&format=json&nojsoncallback=1"
}



