//
//  PixelImage.swift
//  Pixel City
//
//  Created by Olimjon Turgunov on 23.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import Foundation

struct PixelImage {

    // MARK: - Image Metadata
    public private(set) var imageTitle: String?
    public private(set) var imageDescription: String?
    public private(set) var postedDate: String?
    
    // MARK: - Location Data
    public private(set) var latitude: String?
    public private(set) var longitude: String?

}
